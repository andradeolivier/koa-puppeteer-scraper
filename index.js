const Koa = require('koa');
const app = new Koa();
const puppeteer = require('puppeteer');
const request = require('request-promise');

app.use(async ctx => {

    if(ctx.query.url || ctx.request.url){
        let url = null
        if(ctx.query.url){
            url = ctx.query.url;
        } else if(ctx.request.url){
            url = ctx.request.url;
        }

        if(url) {
            try{
                const result = await request.get(url);
                ctx.body = result;
            } catch(e){

                const statusCode = e.statusCode;
                console.log('Status code: '+statusCode);
                const browser = await puppeteer.launch();
                const page = await browser.newPage();
                await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');
                await page.goto(url);
                ctx.body = await page.content();
            }

        } else {
            ctx.body = 'url is missing';
        }

    } else {
        ctx.body = 'url is missing';
    }
});

app.listen(3004);
